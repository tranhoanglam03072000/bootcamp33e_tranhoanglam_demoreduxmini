import React, { Component } from "react";
import { connect } from "react-redux";
import { handleChangeNumberAction } from "./redux/action/numberAction";

class DemoReduxMini extends Component {
  render() {
    return (
      <div>
        <button
          onClick={() => this.props.handleChangeNumber(-1)}
          className="btn btn-danger"
        >
          Giảm
        </button>
        <span className="display-4">{this.props.number}</span>
        <button
          onClick={() => this.props.handleChangeNumber(1)}
          className="btn btn-success"
        >
          Tăng
        </button>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    number: state.numberReducer.number,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    handleChangeNumber: (number) => {
      dispatch(handleChangeNumberAction(number));
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(DemoReduxMini);
