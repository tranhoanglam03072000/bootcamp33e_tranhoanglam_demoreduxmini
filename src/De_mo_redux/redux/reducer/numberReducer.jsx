import { CHANGE_NUMBER } from "../constant/numberConstant";

const initial = {
  number: 1,
};

export const numberReducer = (state = initial, { type, payload }) => {
  switch (type) {
    case CHANGE_NUMBER: {
      state.number += payload;
      return { ...state };
    }
    default:
      return { ...state };
  }
};
