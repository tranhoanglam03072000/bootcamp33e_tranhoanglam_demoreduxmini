import { CHANGE_NUMBER } from "../constant/numberConstant";

export const handleChangeNumberAction = (number) => {
  console.log("yes");
  return {
    type: CHANGE_NUMBER,
    payload: number,
  };
};
